# CS User Support Working Group (CSUSWG)

Collaborate on the evaluation, selection, deployment and maintenance of User Support systems and services requested by the David R. Cheriton School of Computer Science.

## Useful Links

* [CSCF Help Desk](https://uwaterloo.ca/computer-science-computing-facility/getting-help/help-desk)
* [CSCF Teaching Labs](https://cs.uwaterloo.ca/cscf/teaching/labs)
* [CSCF Teaching Hosts](https://uwaterloo.ca/computer-science-computing-facility/teaching-hosts) 
* [Computer Science Linux Working Group](https://cs.uwaterloo.ca/twiki/view/CF/CSLinuxWorkingGroup) 

## Current Members
* cscfhelp
* cogradab
* echrzano
* snickers
* dmerner
* yc2lee
* fhgunn
* twlichty
* omnafees

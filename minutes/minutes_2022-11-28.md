## Meeting Particulars
Start Time: 1100

End Time: 1200

Location: MS Teams

Invited: cogradab, echrzano, pbeldows, ldpaniak, d2shantz, l22frase,  
			kbeckman, omnafees, snickers, dlgawley, tcaduro, ijmorland

Attendees: cogradab, echrzano, snickers, omnafees, tcaduro, pbeldows, d2shantz, ijmorland
		

## TOPICS 


	-- A/V use in CS (O)
		-- Rooms that require regular support (from CS Exec)
			-- Seminar rooms: DC1302, DC1304, DC2568, DC2585
			-- Meeting rooms: DC1331, DC2310, DC2314, DC2564 

		-- Review of existing support systems and services
			-- DC2568, DC2585: Regularly scheduled grad seminars, smooth so far.
			-- DC2585: Wireless mic is missing. Should the wireless receiver be removed ?
					Do we still need the mic ? Staff generally try to keep the doors closed when
					it isn't in use, though this is difficult to rely on as these 
					are high use rooms.
					Suggestion: door closures on the doors ? 
					=> O will investigate where the mic went.
					=> Getting info from cs exec on how to proceed as mic is lost item.
			-- Signage ?
			-- Phone numbers ? 
					=> We need to review the signs in these rooms
					=> Balancing emergency A/V problem solving with pre-event preparation
						-- Can it be made part of the booking process to recommend (and book) 
							testing before the event, at least an hour, 
							especially for sensitive events like thesis defences, etc. ?
						-- generally people request testing, and it would be good to be proactive 
						   with this on our end.
						=> Lew: CS Main office front desk manages bookings - let's provide them with
						   these instructions; O will provide info.
					=> Need to review emergency phone numbers with the signage

		-- Review of user requests
			-- Seminar rooms are for public events as well as internal grad seminars
			-- Meeting rooms are for any type of meeting
			-- Survey ?
				=> Once a term might be too much to respond effectively to.
				=> O will query CS Exec on this.
			

		-- Open RTs with INF (dlgawley)
		https://rt.uwaterloo.ca/Ticket/Display.html?id=1065681 
		https://rt.uwaterloo.ca/Ticket/Display.html?id=1021833

		-- Working with Dutch from ITMS on failing speaker systems in DC1302. 
			=> We need to book the room 
			=> The system is "offline". 
			=> ideally, cscfhelp team, omnafees should be present in DC1302 with
			ITMS. cogradb will coordinate a meet up ITMS.
		-- DC1304 speakers have never been used as it is a smaller room. Do they work ?
			=> testing required. 

		-- A/V loaning equipment (i.e.: DC1301 - projector and screen)
			=> Request from Registrar
			=> If we have the equipment, and if the room is booked by the CS Main Office
			(which is usually the case for DC1300 rooms), we end up being accountable to
			help with A/V in either a partial or 100% capacity.
			=> The equipment is due for evergreeing as well.
			=> We will do a quick test of the projector to make sure it turns on
			=> User should be encouraged to test equipment with our help for their use case.


	-- CSCF Equipment Loaner Pool in DC 2608, the CSCF Help Desk (O)
		-- Useful service that is experiencing more demands
			-- Laptops to undergraduate students
			-- Meeting room users want mics
		-- Types of Equipment ? 
			-- Anything available in DC2608: Laptops, A/V equipment
		-- Loan terms ?
			-- We decide based on the request and availability of the equipment
			-- We're defining "long", "medium" and "short" term based on needs and availability
		-- The library has experience loaning for short-term, mobile equipment 
			=> they are open at non-regular hours for emergency loans
		-- Met with CS145 instructor support and they are interested in borrowing up to 14 laptops.
			=> still waiting for actual request
			=> laptops also can be imaged with a Windows OS.
		-- Power adapters for loaner pool laptops
			-- People borrow them, either with laptops or on their own
			-- Easy to stock
			-- We ought to barcade laptop associated power supplies with the same barcode
			as the laptops?
			-- Having a separate barcoded loaner pool may be a more feasible solution.
			=> we'll establish a small pool for power supplies
			 

	-- O365 licenses associated with the cscfhelp account
		=> we should avoid using generic accounts to enable very short-term visitors
		to access enterprise services. Rather, the sponsor of the visitor should be
		helped to use equipment with their userid to access any enterprise services.
		(e.g. Faculty sponsor for a DLS speaker)

		
	-- Round the table for other possible items of discussion
		-- DC2561 can use some reorg to better serve needs of cscfhelp 
		and grad equipment
			-- suggestions welcome, pass on to pbeldows
			=> cscfhelp will take a look and compile some ideas
			=> rsg needs to be brought in on this as well as there are two
			mobile racks that are research related
		-- DC2583A appears a bit cramped
 			=> contents already being monitored by rsg to facilitate 
			currently occurring renos and equipment moves/transitions for research labs
			(e.g. vision)


## NEW TOPICS AND UPDATES 


	-- Mobile devices for Library (echrzano)




## FOR ARCHIVING -- start this section blank

